using UnityEngine;

public class NetworkCharacter : Photon.MonoBehaviour
{

	RhythmGame game;
	IratulLoop iratulLoop;
	bool iratulMode = false;

	void Awake()
	{
		game = GameObject.Find("Scripts").GetComponent<RhythmGame>();
	}

	void Start()
	{
		//game = GameObject.Find("Scripts").GetComponent<RhythmGame>();
		iratulLoop = GameObject.Find("IratulObject").GetComponent<IratulLoop>();
		game.run = true;
		if (PhotonNetwork.isMasterClient)
		{
			game.iratulMode = true;
			iratulMode = true;
			iratulLoop.run = true;
			GameObject.Find("UI").SendMessage("PlayNewMusic");
		}
		else
			iratulLoop.run = false;
	}

    // Update is called once per frame
    void Update()
    {
		if (iratulMode)
		{
			iratulLoop.run = true;
		}
		else
			iratulLoop.run = false;
    }

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			// We own this player: send the others our data
			stream.SendNext(game.score);
			stream.SendNext(game.combo);
			stream.SendNext(game.multiplier);
		}
		else
		{
				int score = (int)stream.ReceiveNext();
				game.SetScore(info.sender.ID,score);
				//Debug.Log(score);
				/*Debug.Log((int)stream.ReceiveNext());
				Debug.Log((int)stream.ReceiveNext());*/
		}
	}
}
