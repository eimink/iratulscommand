﻿using UnityEngine;
using System.Collections.Generic;

public enum SoundCategory {
	PLEASED,
	DISPLEASED,
	COMMENT,
	JOIN,
	COMBOBREAK,
	COMBO,
	DRUM
}

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {

	public int numOfAudioSources = 4;
	public UnityEngine.Audio.AudioMixerGroup mixer = null;

	public List<AudioClip> pleasedSounds;
	public List<AudioClip> displeasedSounds;
	public List<AudioClip> comments;
	public List<AudioClip> playerJoinSounds;
	public List<AudioClip> comboBreakerSounds;
	public List<AudioClip> comboSounds;
	public List<AudioClip> drumSounds;


	private AudioSource[] sources;

	AudioSource source;

	void Awake()
	{
		//source = this.GetComponent<AudioSource>();
		sources = new AudioSource[numOfAudioSources];
		for(int i = 0; i < numOfAudioSources; i++)
		{
			sources[i] = gameObject.AddComponent<AudioSource>();
			sources[i].playOnAwake = false;
			sources[i].loop = false;
			sources[i].outputAudioMixerGroup = mixer;
		}
	}

	AudioSource FindFreeSource()
	{
		for(int i = 0; i < numOfAudioSources; i++)
		{
			Debug.Log(sources[i]);
			if (!sources[i].isPlaying)
				return sources[i];
		}
		sources[0].Stop();
		return sources[0];
	}

	void PlaySound(SoundCategory category)
	{
		source = FindFreeSource();
		switch (category)
		{
			case SoundCategory.PLEASED:
				if (pleasedSounds.Count > 0)
					source.PlayOneShot(pleasedSounds[Random.Range(0,pleasedSounds.Count)]);
				break;
			case SoundCategory.DISPLEASED:
				if (displeasedSounds.Count > 0)
					source.PlayOneShot(displeasedSounds[Random.Range(0,displeasedSounds.Count)]);
				break;
			case SoundCategory.COMMENT:
				if (comments.Count > 0)
					source.PlayOneShot(comments[Random.Range(0,comments.Count)]);
				break;
			case SoundCategory.JOIN:
				if (playerJoinSounds.Count > 0)
					source.PlayOneShot(playerJoinSounds[Random.Range(0,playerJoinSounds.Count)]);
				break;
			case SoundCategory.COMBOBREAK:
				if (comboBreakerSounds.Count > 0)
					source.PlayOneShot(comboBreakerSounds[Random.Range(0,comboBreakerSounds.Count)]);
				break;
			case SoundCategory.COMBO:
				if (comboSounds.Count > 0)
					source.PlayOneShot(comboSounds[Random.Range(0,comboSounds.Count)]);
				break;
			case SoundCategory.DRUM:
				if (drumSounds.Count > 0)
					source.PlayOneShot(drumSounds[Random.Range(0,drumSounds.Count)]);
				break;
		}
	}

}
