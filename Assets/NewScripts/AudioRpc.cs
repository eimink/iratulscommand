using UnityEngine;

public class AudioRpc : Photon.MonoBehaviour
{

    public AudioClip marco;
    public AudioClip polo;

    AudioSource m_Source;

    void Awake()
    {
        m_Source = GetComponent<AudioSource>();
    }

	[PunRPC]
	public void ScoreSent(int param)
	{
		int s = (int)param;
		Debug.Log("Score reported: " +s);
	}

    [PunRPC]
    void SuccesfulCombo()
    {
        if( !this.enabled )
        {
            return;
        }

        Debug.Log( "Success" );

        m_Source.clip = marco;
        m_Source.Play();
    }

    [PunRPC]
    void FailedCombo()
    {
        if( !this.enabled )
        {
            return;
        }

        Debug.Log( "Fail!" );

        m_Source.clip = polo;
        m_Source.Play();
    }

    void OnApplicationFocus( bool focus )
    {
        this.enabled = focus;
    }
}
