using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class RhythmGame : MonoBehaviour {

	public GameObject GameUI;
	public GameObject comboBreakerText;
	public GameObject comboBreakerMirroredText;
	public GameLogic GameLogic;
	public GameObject IratulAnim;
	public int score = 0;
	public int multiplier = 1;
	public int combo = 0;
	public bool run = false;
	public bool prepare = true;
	public float speedFactor = 1.0f;
	private float lastSpawnTime = 1f;
	public bool iratulMode = false;
	private static PhotonView ScenePhotonView;
	private float roundDuration = 30f;
	private float lastResetTime = 1f;
	public int counter = 3;
	private int challengeMultiplier = 1;
	public Dictionary<int,int> scores = new Dictionary<int, int>();


	public void SetScore(int id, int score)
	{
		if(scores.Keys.Contains(id))
		{
			scores[id] = score;
		}
		else
		{
			scores.Add(id, score);
		}
	}

	// Use this for initialization
	public void Start()
	{
		ScenePhotonView = this.GetComponent<PhotonView>();
		//GameUI = GameObject.Find("Scripts");

	}
	
	// Update is called once per frame
	void Update () {
		if (run)
		{
			if (!iratulMode)
			{
				if(!GameUI.GetActive())
					GameUI.SetActive(true);
			}
			else
			{
				if(GameUI.GetActive())
					GameUI.SetActive(false);
			}
			if (prepare)
			{
				if(Time.time - lastResetTime > 1)
				{
					counter--;
					Debug.Log(counter);
					lastResetTime = Time.time;
				}
				if (counter <= 0)
					prepare = false;
			}
			else
			{
				if (!iratulMode)
				{
					if (speedFactor <= 0)
						speedFactor = 1;
					if (Time.time - lastSpawnTime > 1 / speedFactor)
					{
						lastSpawnTime = Time.time;
						SendMessage("SpawnRandomTargetCircle");
					}
				}
				else
				{

					if (Time.time - lastResetTime > roundDuration)
					{
						if (IratulIsPleased())
						{
							Debug.LogError("PLEASED!");
							SendMessage("PlaySound",SoundCategory.PLEASED);
							IratulAnim.SendMessage("StartYShake");
							challengeMultiplier++;
						}
						else
						{
							Debug.LogError("DISPLEASED!");

							SendMessage("PlaySound",SoundCategory.DISPLEASED);
							IratulAnim.SendMessage("StartYShake");
							challengeMultiplier = 1;
						}
						lastResetTime = Time.time;
						GameLogic.SetMinigame(1);
					}
				}
			}
		}
		else
		{
			lastResetTime = Time.time;
		}
	}

	bool IratulIsPleased()
	{
		int total = 0;
		int count = 0;
		foreach(KeyValuePair<int, int> score in scores)
		{
			total += score.Value;
			count++;
		}
		Debug.LogError("Total score: "+total);
		if (count == 0)
			return false;
		return total/(count) > 3000*challengeMultiplier;
	}

	void IncrementScore(float s)
	{
		SendMessage("PlaySound",SoundCategory.DRUM);
		combo++;
		int convertedScore = Mathf.FloorToInt(s*10)*10;
		if (combo > 0 && combo % 10 == 0 && multiplier < 10)
		{
			multiplier++;
			Debug.Log("Multiplier x"+multiplier);
			SendMessage("PlaySound",SoundCategory.COMBO);
		}
		score += (multiplier * convertedScore);
		SendMessage("UpdateScore",score);
	}
		
	void ResetCombo()
	{
		if (combo > 0)
		{
			combo = 0;
			multiplier = 1;
			Vector3 pos = new Vector3(0f,0f,-6f);
			GameObject.Instantiate(comboBreakerText, pos, Quaternion.identity);
			GameLogic.ScenePhotonView.RPC("ResetRemoteCombo", PhotonTargets.All);
		}
	}

	[PunRPC]
	void ResetRemoteCombo()
	{
		if(iratulMode)
		{
			Vector3 pos = new Vector3(18.6f,26.98f,1f);
			Quaternion rot = Quaternion.Euler(new Vector3(0,-180f,0));
			GameObject.Instantiate(comboBreakerMirroredText, pos, rot);
			SendMessage("PlaySound",SoundCategory.COMBOBREAK);
			IratulAnim.SendMessage("StartYShake");
		}
	}

	[PunRPC]
	public void MinigameSet()
	{
		object[] rinkulat = GameObject.FindGameObjectsWithTag("rinkula");
		for ( int i = 0; i < rinkulat.Length; i++)
			Destroy((GameObject)rinkulat[i]);
		combo = 0;
		multiplier = 1;
		score = 0;
		counter = 3;
		prepare = true;
		Debug.Log("MinigameSet");
		SendMessage("UpdateScore",score);
	}
		
}
