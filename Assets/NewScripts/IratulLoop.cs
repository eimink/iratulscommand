﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IratulLoop : MonoBehaviour {

	public GameObject iratulCamera;
	public GameObject iratulPlanes;
	public GameObject singlePlane;
	public bool run = false;
	private bool projectorMode = true;

	// Use this for initialization
	void Start () {
		projectorMode = GameObject.Find("UI").GetComponent<StartOptions>().projectorMode;
		if (projectorMode)
		{
			iratulPlanes.SetActive(true);
			singlePlane.SetActive(false);
		}
		else
		{
			singlePlane.SetActive(true);
			iratulPlanes.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (run)
		{
			if (!iratulCamera.GetActive())
			{
				iratulCamera.SetActive(true);
				if (projectorMode)
				{
					iratulPlanes.SetActive(true);
					singlePlane.SetActive(false);
				}
				else
				{
					singlePlane.SetActive(true);
					iratulPlanes.SetActive(false);
				}
			}
		}
		else 
		{
			if (iratulCamera.GetActive())
			{
				iratulCamera.SetActive(false);
				iratulPlanes.SetActive(false);
				singlePlane.SetActive(false);
			}
		}
	
	}


}
