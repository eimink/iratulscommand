﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class RhythmGameUI : MonoBehaviour {

	public GameObject targetCircle;
	public TextMesh scoreText;
	public List<GameObject> touchAreas;


	// Use this for initialization
	void Start () {
		UnityEngine.Random.seed = System.DateTime.Now.Millisecond;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void SpawnRandomTargetCircle()
	{
		SpawnTargetCircle(UnityEngine.Random.Range(0,touchAreas.Count));
	}

	void SpawnTargetCircle(int position)
	{
		GameObject tc;
		if (targetCircle != null)
		{
			tc = (GameObject)GameObject.Instantiate(targetCircle, touchAreas[position].transform.position, Quaternion.identity);
			tc.GetComponent<TargetCircle>().targetSprite = touchAreas[position];
			tc.GetComponent<TargetCircle>().rhythmGame = GameObject.Find("Scripts");
		}
	}

	void UpdateScore(int score)
	{
		scoreText.text = score.ToString();
	}
		
}
