﻿using UnityEngine;
using System.Collections;

public class AlphaFade : MonoBehaviour {

	public float minimum = 1.0f;
	public float maximum = 0f;
	public float duration = 1.5f;
	private float startTime = 0f;
	public SpriteRenderer sprite;

	void Start() 
	{
		startTime = Time.time;
	}

	// Update is called once per frame
	void Update () 
	{
		float t = (Time.time - startTime) / duration;
		sprite.color = new Color(1f,1f,1f,Mathf.SmoothStep(minimum, maximum, t)); 
	}

	void ResetFade()
	{
		startTime = Time.time;
	}
}