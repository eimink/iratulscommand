﻿using UnityEngine;
using System.Collections;

public class TargetCircle : MonoBehaviour {

	public GameObject targetSprite;
	public GameObject rhythmGame;

	bool wasClicked = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		/*for (var i = 0; i < Input.touchCount; ++i) {
			if (Input.GetTouch(i).phase == TouchPhase.Began) {
				RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position), Vector2.zero);
				// RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
				if(hitInfo)
				{
					Debug.Log( hitInfo.transform.gameObject.name );
					// Here you can check hitInfo to see which collider has been hit, and act appropriately.
				}
			}
		}
		if (Input.GetMouseButtonDown (0)) {
			Debug.Log ("Clicked");
			Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(pos), Vector2.zero);
			// RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
			if(hitInfo)
			{
				Debug.Log( hitInfo.transform.gameObject.name );
				// Here you can check hitInfo to see which collider has been hit, and act appropriately.
			}
		}*/
	}

	void OnMouseDown() {
		Debug.Log ("Clicked");
		if (targetSprite != null)
			targetSprite.SendMessage("TriggerGlow",SendMessageOptions.DontRequireReceiver);
		rhythmGame.SendMessage("IncrementScore",1f-this.gameObject.transform.localScale.x);
		wasClicked = true;
		Destroy(this.gameObject);
		//gameObject.GetComponentInChildren<AlphaFade>().SendMessage("ResetFade");
	}

	void OnDestroy()
	{
		if (!wasClicked)
		{
			rhythmGame.SendMessage("ResetCombo");
		}
	}
}
