﻿using UnityEngine;
using System.Collections;

public class IratulAnim : MonoBehaviour {

	public float movementX = 1.0f;
	public float movementY = 0.5f;
	private Vector3 pos1 = new Vector3(-4,0,0);
	private Vector3 pos2 = new Vector3(4,0,0);
	public float speed = 1.0f;
	public float effectDuration = 0.25f;
	public float effectStart = 0;
	public bool runEffect = false;
	private float originY;
	private float fxY1;
	private float fxY2;


	void Start() {
		pos1 = new Vector3(this.transform.position.x - movementX, this.transform.position.y, this.transform.position.z);
		pos2 = new Vector3(this.transform.position.x + movementX, this.transform.position.y, this.transform.position.z);
		originY = this.transform.position.y;
		fxY1 = originY - movementY;
		fxY2 = originY + movementY;
		effectStart = Time.time;
	}

	public void StartYShake()
	{
		effectStart = Time.time;
		runEffect = true;
	}

	void Update() {
		Vector3 position = Vector3.Lerp (pos1, pos2, (Mathf.Sin(speed * Time.time) + 1.0f) / 2.0f);
		if (Time.time - effectStart > effectDuration)
		{
			runEffect = false;
		}
		if (runEffect)
		{
			position.y = Mathf.Lerp(originY,fxY2,(Mathf.Sin(speed * Time.time * 100) + 1f) / 2.0f);
		}
		else
		{
			position.y = Mathf.Lerp(this.transform.position.y, originY,speed * Time.time + 0.1f);
		}
		transform.position = position;
	}
}
