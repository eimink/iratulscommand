using UnityEngine;

public class RandomMatchmaker : Photon.PunBehaviour
{
    private PhotonView myPhotonView;

    // Use this for initialization
    public void Start()
    {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
        PhotonNetwork.ConnectUsingSettings("0.1");
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("JoinRandom");
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnConnectedToMaster()
    {
        // when AutoJoinLobby is off, this method gets called when PUN finished the connection (instead of OnJoinedLobby())
        PhotonNetwork.JoinRandomRoom();
    }

    public void OnPhotonRandomJoinFailed()
    {
        PhotonNetwork.CreateRoom(null);
    }

    public override void OnJoinedRoom()
    {
        GameObject monster = PhotonNetwork.Instantiate("playerprefab", Vector3.zero, Quaternion.identity, 0);
        myPhotonView = monster.GetComponent<PhotonView>();
    }

	public void OnMinigameCompleted()
	{
		if (PhotonNetwork.connectionStateDetailed == PeerState.Joined)
		{
			int score = 100;
			myPhotonView.RPC("ScoreSent", PhotonTargets.MasterClient, score);
		}
	}

	public void OnSuccesfulCombo()
	{
		if (PhotonNetwork.connectionStateDetailed == PeerState.Joined)
		{
			myPhotonView.RPC("SuccesfulCombo", PhotonTargets.All);
		}
	}

	public void OnFailedCombo()
	{
		if (PhotonNetwork.connectionStateDetailed == PeerState.Joined)
		{
			myPhotonView.RPC("FailedCombo", PhotonTargets.All);
		}
	}

    public void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());

        /*if (PhotonNetwork.connectionStateDetailed == PeerState.Joined)
        {
            bool shoutMarco = GameLogic.playerWhoIsIt == PhotonNetwork.player.ID;

            if (GUILayout.Button("Success!"))
            {
				OnSuccesfulCombo();
            }
            if (GUILayout.Button("Fail!"))
            {
				OnFailedCombo();
            }
			if (GUILayout.Button("EndMiniGame!"))
			{
				OnMinigameCompleted();
			}
        }*/
    }
}
