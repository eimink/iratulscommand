﻿using UnityEngine;
using System.Collections;

public class ScaleFade : MonoBehaviour {

	public float minimum = 1.0f;
	public float maximum = 0f;
	public float duration = 1.5f;
	public bool destroyOnFinish = true;
	private float startTime;
	private float step;
	void Start() 
	{
		startTime = Time.time;
		if(destroyOnFinish)
			GameObject.Destroy(this.gameObject,duration);
	}

	// Update is called once per frame
	void Update () 
	{
		float t = (Time.time - startTime) / duration;
		step = Mathf.SmoothStep(minimum,maximum,t);
		Vector3 scale = new Vector3(step,step,1);
		this.gameObject.transform.localScale = scale;
		//sprite.color = new Color(1f,1f,1f,Mathf.SmoothStep(minimum, maximum, t)); 
	}

	void ResetFade()
	{
		startTime = Time.time;
	}
}